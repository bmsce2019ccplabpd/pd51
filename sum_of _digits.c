
#include <stdio.h>
int sum_of_digits(int p)
{
     int x,temp,sum=0;
     while(p>0)
     {
         temp=p%10;
         sum=sum+temp;
         p=p/10;
     }
     return sum;
}
int main()
{
     int sum,x;
     printf("enter the value of number:\n");
     scanf("%d",&x);
     sum=sum_of_digits(x);
     printf("sum of digits of given number is %d\n",sum);
     return 0;
}    
 