#include<stdio.h>
void swap(int *,int *);
int main()
{
    int a,b;

    printf("enter the numbers:");
    scanf("%d%d",&a,&b);
    printf("In main a=%d and b=%d\n",a,b);
    swap(&a,&b);
    printf("The swapped numbers are a=%d and b=%d\n",a,b);
    return 0;
}
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
    printf("In function a=%d and b=%d\n",*a,*b);
}         
